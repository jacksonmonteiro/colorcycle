let red = document.getElementById('red');
let green = document.getElementById('green');
let blue = document.getElementById('blue');

const applyButton = document.getElementById('apply');
const box = document.getElementById('result')

applyButton.onclick = function() {
	box.style.background = `rgb(${red.value},${green.value},${blue.value})`;
}